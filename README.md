# Synonym Dictionary To Taxonomy Skills 

The purpose of this repository is to find the most common connected concept between synonym dictionary and taxonomy skills to help Redaction team since Redaktionen team needs to map like 40 000 skills. For that first we get sorted enriched jobads skill and synonym dictionary.  Then find connected concept between sorted enriched jobads skill and synonym dictionary. Finally find connected concept between synonym dictionary and taxonomy skill preferred label.

## Set up environment:

- Make sure python 3.11 is installed.
- Make sure pip is installed.
- Set up a venv(IntelliJ or similar environment):
    - File -> Project Structure -> SDKs -> + -> Add Python SDK... -> Virtual ENV Environment
- Install dependencies:
  ```pip install - r requirements.txt```

## Expected result:

First run ```synonymdic_enrichedjobads.py``` to get ```enriched_jobads_skill_output.ods``` and ```synonym_dictionary_output.ods```.

Then run ```cc_enrichedjobadsskill_synonymdictionary.py``` to get ```connected_concepts_enrichedjobadsskill_synonymdictionary.ods```.

Finally run ```main.py``` to get ```connected_concepts_enrichedjobadsskill_synonymdictionary_taxskill.ods```.

