from pathlib import Path
import pandas as pd
import pyexcel_ods
import common

def get_skill_concept():
    skill_file = str(Path(__file__).parent.joinpath('../data/skills.json'))
    skill_file = common.get_taxonomy_concepts_json(skill_file)
    skill_concepts = skill_file["data"]["concepts"]
    return skill_concepts

def get_ods_data(file_name='../data/connected_concepts_enrichedjobadsskill_synonymdictionary.ods'):
    connected_concepts_jobadsskill_syndic = pd.read_excel(file_name,
                                       usecols=['Enriched Jobads Skill', 'Synonym Dictionary', 'Distance'])
    connected_concepts_jobadsskill_syndic = [value for value in connected_concepts_jobadsskill_syndic.values]
    return connected_concepts_jobadsskill_syndic

def get_connected_concepts_syndic(data):
    return [list_data[1] for list_data in data]

def get_connected_concepts_jobadsskill(data):
    return [list_data[0] for list_data in data]

def connected_concepts_dic_skill(syndic, skill_labels,  indices, distances, concept_ids):
    result_list = []
    for i, item in enumerate(syndic):
        result = {}
        output_list = []
        for (index, distance) in zip(indices[i, :], distances[i, :]):
            label = skill_labels[index]
            output_list.append({"id": concept_ids[label], "label": label, "distance": str(distance)})
        result[item] = output_list
        result_list.append(result)
    return result_list

def get_connected_concepts_skill(connected_concepts_syndic, skill):
    skill_labels = common.get_lowercase_preferred_labels(skill)
    concept_ids = common.concept_ids_dic(skill_labels, skill)
    syndic = common.get_lowercase(connected_concepts_syndic)
    indices, distances = common.nearest_neighbors(syndic, skill_labels)
    connected_concepts = connected_concepts_dic_skill(syndic, skill_labels, indices, distances, concept_ids)
    return connected_concepts

def render_table_synonymdictionary_taxskill(out_filename_ods, connected_concepts_syndic_taxskill, enriched_jobads_skill):
    dst = [["Enriched Jobads Skill", "Synonym Dictionary", "Tax Skill Preferred Label", "Tax Skill Id", "Distance"]]
    for dic, jobads_skill in zip(connected_concepts_syndic_taxskill, enriched_jobads_skill):
        for key in dic.keys():
            for values in dic[key]:
                dst.append([jobads_skill, key, values["label"], values["id"], values["distance"]])
    pyexcel_ods.save_data(out_filename_ods, {None: dst})

def main():
    skill_concepts = get_skill_concept()
    data = get_ods_data(file_name='../data/connected_concepts_enrichedjobadsskill_synonymdictionary.ods')
    connected_concepts_syndic = get_connected_concepts_syndic(data)
    connected_concepts_jobadsskill = get_connected_concepts_jobadsskill(data)
    connected_concepts_syndic_taxskill = get_connected_concepts_skill(connected_concepts_syndic, skill_concepts)
    print(connected_concepts_syndic_taxskill)
    render_table_synonymdictionary_taxskill("../data/connected_concepts_enrichedjobadsskill_synonymdictionary_taxskill.ods",
                                                    connected_concepts_syndic_taxskill, connected_concepts_jobadsskill)

if __name__ == "__main__":
    main()
    print("connected_concepts_enrichedjobadsskill_synonymdictionary_taxskill.ods done!")

