from sentence_transformers import SentenceTransformer
from sklearn.neighbors import NearestNeighbors
import numpy as np
import time
import json

model = SentenceTransformer("KBLab/sentence-bert-swedish-cased")

def get_taxonomy_concepts_json(filename):
    with open(filename, encoding="utf8") as f:
        return json.load(f)

def flatten(xss):
    return [x for xs in xss for x in xs]

def get_lowercase(tax_list):
    return [label.lower() for label in tax_list]

def normalize(x):
    return x / np.linalg.norm(x)

def encode_vectors(labels_list):
    progress = ProgressReporter(len(labels_list))
    dst = []
    for label in labels_list:
        progress.end_of_iteration_message("Encoding...")
        dst.append(normalize(model.encode(label)))
    return dst

def get_preferred_labels(concept):
    return [label for labels in [[concept["preferred_label"]]] for label in labels]

def get_lowercase_preferred_labels(tax_list):
    tax_labels = [get_preferred_labels(concept) for concept in tax_list]
    return [label.lower() for label in flatten(tax_labels)]

def concept_ids_dic(tax_labels, tax_list):
    dic_id = {}
    label_set = set(tax_labels)
    for concept in tax_list:
        for label in get_lowercase_preferred_labels([concept]):
            if label in label_set:
                dic_id[label] = concept["id"]
    return dic_id

def nearest_neighbors(jobads_skill, synonym_dictionary):
    vectors = encode_vectors(synonym_dictionary)
    neighbors = NearestNeighbors(n_neighbors=5).fit(vectors)
    distances, indices = neighbors.kneighbors(encode_vectors(jobads_skill))
    print("distance shape is {:s}, indices shape is {:s}".format(str(distances.shape), str(indices.shape)))
    return indices, distances

def compute_time_estimate(progress, elapsed_time):
    base = {"elapsed_time": elapsed_time,
            "progress": progress}
    if progress <= 0:
        return base
    time_per_progress = elapsed_time/progress
    remaining_progress = 1.0 - progress
    remaining_time = remaining_progress*time_per_progress
    return {**base,
            "time_per_progress": time_per_progress,
            "total_time": time_per_progress,
            "remaining_progress": remaining_progress,
            "remaining_time": remaining_time}

time_segmentation_seconds_breakdown = ["weeks", 7, "days", 24, "hours", 60, "minutes", 60, "seconds"]

def segment_time(t, segmentation=time_segmentation_seconds_breakdown):
    seg = list(reversed(segmentation))
    parts = []
    x = t
    while True:
        n = len(seg)
        unit = seg[0]
        if n == 1:
            parts.append((x, unit))
            break;
        else:
            d = seg[1]
            seg = seg[2:]
            xi = int(round(x))
            y = xi % d
            x = xi // d
            if 0 < y or (len(parts) == 0 and x == 0):
                parts.append((y, unit))
        if x == 0:
            break
    return parts

def format_time_segmentation(time_parts):
    n = min(2, len(time_parts))
    return ", ".join(map(lambda p: "{:d} {:s}".format(p[0], p[1]), reversed(time_parts[-n:])))

def format_seconds(seconds):
    return format_time_segmentation(segment_time(seconds))

def format_time_estimate(est):
    s = "Progress: {:d} %".format(int(round(100*est["progress"])))

    def timeinfo(k, lab):
        if k in est:
            return "\n{:s}: {:s}".format(lab, format_seconds(est[k]))
        return ""
    return s + timeinfo("total_time", "Total") + timeinfo("elapsed_time", "Elapsed") + timeinfo("remaining_time", "Remaining")

class ProgressReporter:
    def __init__(self, total_iterations, completed_iterations = 0, elapsed = 0):
        self.start = time.time() - elapsed
        self.total_iterations = total_iterations
        self.completed_iterations = completed_iterations
        self.rate_limiter = rate_limiter(1)

    def set_start(self, start):
        self.start = start

    def end_of_iteration(self):
        self.completed_iterations += 1

    def time_estimate(self):
        return compute_time_estimate(self.completed_iterations/self.total_iterations, time.time() - self.start)

    def elapsed(self):
        return time.time() - self.start

    def progress_report(self):
        return "Completed {:d} of {:d} iterations\n".format(
            self.completed_iterations, self.total_iterations) + format_time_estimate(self.time_estimate())

    def end_of_iteration_message(self, msg):
        self.end_of_iteration()
        if self.rate_limiter():
            print(msg)
            print(self.progress_report() + "\n")

    def to_data(self):
        return {"elapsed": self.elapsed(),
                "total_iterations": self.total_iterations,
                "completed_iterations": self.completed_iterations}

def rate_limiter(period=1):
    last = None

    def f():
        nonlocal last
        t = time.time()
        if (last is None) or (period + last) < t:
            last = t
            return True
        return False
    return f


