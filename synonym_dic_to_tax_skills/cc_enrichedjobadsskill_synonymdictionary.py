import pandas as pd
import pyexcel_ods
import common

def get_synonym_dictionary():
    synonym_dictionary = '../data/synonym_dictionary_output.ods'
    synonym_dictionary = pd.read_excel(synonym_dictionary, engine='odf', usecols=['Synonym Dictionary'])
    synonym_dictionary = [list(value) for value in synonym_dictionary.values]
    synonym_dictionary = common.flatten(synonym_dictionary)
    return synonym_dictionary

def get_enriched_jobads_skill():
    enriched_jobads_skill = '../data/enriched_jobads_skill_output.ods'
    enriched_jobads_skill = pd.read_excel(enriched_jobads_skill, engine='odf', usecols=['Enriched Jobads skill', 'Count'])
    enriched_jobads_skill = enriched_jobads_skill.sort_index(ascending=True)
    enriched_jobads_skill = [list(value) for value in enriched_jobads_skill.values]
    enriched_jobads_skill = [list[0] for list in enriched_jobads_skill]
    return enriched_jobads_skill

def connected_concepts_dic(jobads_skill, synonym_dictionary,  indices, distances):
    result = {}
    for i, item in enumerate(jobads_skill):
        output_list = []
        for (index, distance) in zip(indices[i, :], distances[i, :]):
            label = synonym_dictionary[index]
            output_list.append({"label": label, "distance": str(distance)})
        result[item] = output_list
    return result

def get_connected_concepts(jobads_skill, synonym_dictionary):
    jobads_skill = common.get_lowercase(jobads_skill)
    synonym_dictionary = common.get_lowercase(synonym_dictionary)
    indices, distances = common.nearest_neighbors(jobads_skill, synonym_dictionary)
    connected_concepts = connected_concepts_dic(jobads_skill, synonym_dictionary, indices, distances)
    return connected_concepts

def render_table_enrichedjobadsskill_synonymdictionary(out_filename_ods, connected_concepts):
    dst = [["Enriched Jobads Skill", "Synonym Dictionary", "Distance"]]
    for key in connected_concepts.keys():
        for values in connected_concepts[key]:
            dst.append([key, values["label"], values["distance"]])
    pyexcel_ods.save_data(out_filename_ods, {None: dst})

def main():
    synonym_dictionary = get_synonym_dictionary()
    enriched_jobads_skill = get_enriched_jobads_skill()
    connected_concepts_enrichedjobadsskill_synonymdictionary = get_connected_concepts(enriched_jobads_skill,
    synonym_dictionary)
    render_table_enrichedjobadsskill_synonymdictionary("../data/connected_concepts_enrichedjobadsskill_synonymdictionary.ods",
    connected_concepts_enrichedjobadsskill_synonymdictionary)

if __name__ == "__main__":
    main()
    print("connected_concepts_enrichedjobadsskill_synonymdictionary.ods done!")
