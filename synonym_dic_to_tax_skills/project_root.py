from pathlib import Path
import os
os.environ["GIT_PYTHON_REFRESH"] = "quiet"
import git

def get_project_root():
    return Path(git.Repo('.', search_parent_directories=True).working_tree_dir)
