from project_root import get_project_root
from io import TextIOWrapper
from zipfile import ZipFile
import pandas as pd
import pyexcel_ods
import common
import csv
import ast
import os

root = get_project_root()
#path = "root/data/ontologi_verified"
path = root/"data"/"ontologi_verified"
csv_files = os.listdir(path=path)
print("csv_files Len:", len(csv_files))

def get_all_kompetens_set():
    all_kompetens_list = []
    for csv_file in csv_files:
        read_csv_file = pd.read_csv(root/"data"/"ontologi_verified"/csv_file)
        csv_file_header = [row for row in read_csv_file]
        csv_file_list = [list(row) for row in read_csv_file.values]
        csv_file_list.insert(0, csv_file_header)
        csv_file_list = common.flatten(csv_file_list)
        all_kompetens_list.append(csv_file_list)

    all_kompetens_list = common.flatten(all_kompetens_list)

    improve_kompetens_list = []
    for value in all_kompetens_list:
        if ';;' in value:
            value = value.split(';;')[0]
        improve_kompetens_list.append(value)

    all_kompetens_set = {c for c in improve_kompetens_list}

    print("all_kompetens_list Len:", len(all_kompetens_list))
    print("improve_kompetens_list Len:", len(improve_kompetens_list))
    print("all_kompetens_set Len:", len(all_kompetens_set))
    return all_kompetens_set

def get_enriched_skills():
    with ZipFile('../data/2021_beta1.csv.zip') as zf:
        with zf.open('2021_beta1.csv', 'r') as infile:
            reader = csv.reader(TextIOWrapper(infile, 'utf-8'))
            list_reader = []
            #c=0
            for row in reader:
                list_reader.append(row)
                #c+=1
                #if(c==20):
                  #break;
            #print(list_reader[1:])
            per_skill = {}
            list_skill = []
            for row in list_reader[1:]:
                  row_list = ast.literal_eval(row[17])
                  if 'enriched' in row_list:
                     list_skill.append(row_list['enriched']['skill'])
            list_skill = common.flatten(list_skill)
            for skill in list_skill:
                  s = skill
                  if not (s in per_skill):
                      per_skill[s] = 1
                  else:
                      per_skill[s] += 1

    per_skills = sorted(per_skill.items(), key=lambda x: x[1], reverse=True)
    return per_skills

def render_table_synonym_dictionary_output(filename_ods, synonym_dictionary):
    dst = [["Synonym Dictionary"]]
    for value in synonym_dictionary:
        dst.append([value])
    pyexcel_ods.save_data(filename_ods, {None: dst})

def render_table_enriched_jobads_output(filename_ods, enriched_jobads):
    dst = [["Enriched Jobads skill", "Count"]]
    for value in enriched_jobads:
        dst.append([value[0], value[1]])
    pyexcel_ods.save_data(filename_ods, {None: dst})

def main():
    all_kompetens_set = get_all_kompetens_set()
    render_table_synonym_dictionary_output("../data/synonym_dictionary_output.ods", all_kompetens_set)
    print("synonym_dictionary_output.ods done!")
    enriched_skills = get_enriched_skills()
    render_table_enriched_jobads_output("../data/enriched_jobads_skill_output.ods", enriched_skills)
    print("enriched_jobads_output.ods done!")


if __name__ == "__main__":
    main()




