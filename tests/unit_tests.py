import os, sys
sys.path.insert(0, os.path.abspath("../synonym_dic_to_tax_skills"))
import unittest

class MyTestCase(unittest.TestCase):
    def test_synonym_dictionary_data(self):
        list_sample = ["ansvarsfrågor", "inköpsavdelning", "lönesystem", "affektiv störning", "beslutsstödsystem",
                       "inbyggda system", "insurtech", "bufféhantering", "asfaltframställning", "socialpedagogik",
                       "funktionsvariation", "kundtjänstarbete", "project management office", "projektägare", "abortverksamhet",
                       "arbetstagarorganisationer", "industriellkommunikation", "infotaimentsystem", "tillgångsredovisningar",
                       "rf-behandling", "antällningsstöd", "barnavårdslärarutbildning", "linuxkompetens", "faiveleytransport",
                       "mediebyråtjänster", "ljussättningsprodukter", "latrinhantering", "ackumulatorskötsel", "yield management",
                       "grundläggande tjänstgöring igf-koden", "förarbevis för terränghjuling", "flygtekniska verktyg", "kompensationsodling",
                       "bedömning av reell kompetens", "borrad grundläggning", "kalderash", "överbyggnadsarbeten", "vårdadministrationsutbildning"]
        
        self.assertTrue("bedömning av reell kompetens" in list_sample)
        self.assertTrue("infotaimentsystem" in list_sample)
        self.assertTrue("vårdadministrationsutbildning" in list_sample)
        self.assertTrue("lönesystem" in list_sample)

    def test_render_table_synonym_dictionary_output(self):
        list_sample = ["ansvarsfrågor", "inköpsavdelning", "lönesystem", "affektiv störning", "beslutsstödsystem",
                       "inbyggda system", "insurtech", "bufféhantering", "asfaltframställning", "socialpedagogik",
                       "funktionsvariation", "kundtjänstarbete", "project management office", "projektägare", "abortverksamhet",
                       "arbetstagarorganisationer", "industriellkommunikation", "infotaimentsystem", "tillgångsredovisningar",
                       "rf-behandling", "antällningsstöd", "barnavårdslärarutbildning", "linuxkompetens", "faiveleytransport",
                       "mediebyråtjänster", "ljussättningsprodukter", "latrinhantering", "ackumulatorskötsel", "yield management",
                       "grundläggande tjänstgöring igf-koden", "förarbevis för terränghjuling", "flygtekniska verktyg", "kompensationsodling",
                       "bedömning av reell kompetens", "borrad grundläggning", "kalderash", "överbyggnadsarbeten", "vårdadministrationsutbildning"]
           
        self.assertTrue("överbyggnadsarbeten" in list_sample)
        self.assertTrue("latrinhantering" in list_sample)
        self.assertTrue("abortverksamhet" in list_sample)
        self.assertTrue("rf-behandling" in list_sample)

    def test_enriched_jobads_data(self):
        per_skill = [('telomer', 1), 
                     ('kundsektorn', 1), 
                     ('tillskärningsmaskin', 1), 
                     ('branddetekteringssystem', 1), 
                     ('rstp', 1), 
                     ('osek', 1), 
                     ('beredningsteknik', 1), 
                     ('fritidshemsmiljö', 1), 
                     ('dokumentdelning', 1), 
                     ('finelektronik', 1), 
                     ('derivathandel', 1), 
                     ('modulansvar', 1), 
                     ('bilreparationer', 84), 
                     ('gis-system', 84), 
                     ('systemövervakning', 84), 
                     ('klimatanpassning', 84), 
                     ('yhutbildning', 84), 
                     ('elevhälsofrågor', 83), 
                     ('säljträning', 83)]
        
        self.assertEqual(per_skill[0], ('telomer', 1))
        self.assertEqual(per_skill[18], ('säljträning', 83))
        self.assertEqual(per_skill[4], ('rstp', 1))

    def test_render_table_enriched_jobads_output(self):
        enriched_jobads_skill = ['spärrtjänst', 
                                 'malmtransport', 
                                 'grafikprocessorer', 
                                 'bluetooth-teknik', 
                                 'jax', 
                                 'helhetsprogram', 
                                 'inspirationslösningar', 
                                 'onlineproduktion', 
                                 'behandlingsteknik', 
                                 'rumsservice', 
                                 'bashabilitering', 
                                 'permutation', 
                                 'konferensvärdinneuppgifter', 
                                 'vaktmästarservice', 
                                 'servicerapportering', 
                                 'exportorderhantering', 
                                 'materialsammanställning', 
                                 'friklassningsarbete', 
                                 'övervakningstjänster', 
                                 'organisationsutvecklingsprojekt', 
                                 'samhällsvetarexamen']
        
        self.assertEqual(enriched_jobads_skill[0], 'spärrtjänst')
        self.assertEqual(enriched_jobads_skill[20], 'samhällsvetarexamen')
        self.assertEqual(enriched_jobads_skill[4], 'jax')

    def test_render_table_enrichedjobadsskill_synonymdictionary(self):
        connected_concepts_syndic = ['svenska', 
                                     'stockholmska', 
                                     'rikssvenska', 
                                     'göteborgska', 
                                     'finlandssvenska', 
                                     'körkort', 
                                     'körkortslicens', 
                                     'körbevis', 
                                     'förarlicens', 
                                     'förarbevis']
        
        connected_concepts_jobadsskill = ['svenska', 
                                          'svenska', 
                                          'svenska', 
                                          'svenska', 
                                          'svenska', 
                                          'körkort', 
                                          'körkort', 
                                          'körkort', 
                                          'körkort', 
                                          'körkort']
        
        self.assertEqual(connected_concepts_jobadsskill[0], 'svenska')
        self.assertEqual(connected_concepts_jobadsskill[5], 'körkort')
        self.assertEqual(connected_concepts_syndic[0], 'svenska')
        self.assertEqual(connected_concepts_jobadsskill[5], 'körkort')

    def test_render_table_enrichedjobadsskill_synonymdictionary_taxskills(self):
        connected_concepts_syndic = ['svenska', 
                                     'svenska', 
                                     'svenska', 
                                     'svenska', 
                                     'svenska', 
                                     'stockholmska', 
                                     'stockholmska', 
                                     'stockholmska', 
                                     'stockholmska', 
                                     'stockholmska', 
                                     'rikssvenska', 
                                     'rikssvenska', 
                                     'rikssvenska', 
                                     'rikssvenska', 
                                     'rikssvenska', 
                                     'göteborgska', 
                                     'göteborgska', 
                                     'göteborgska', 
                                     'göteborgska', 
                                     'göteborgska', 
                                     'finlandssvenska', 
                                     'finlandssvenska', 
                                     'finlandssvenska', 
                                     'finlandssvenska', 
                                     'finlandssvenska', 
                                     'körkort', 
                                     'körkort', 
                                     'körkort', 
                                     'körkort', 
                                     'körkort']

        connected_concepts_jobadsskill = ['svenska', 
                                          'svenska', 
                                          'svenska', 
                                          'svenska', 
                                          'svenska', 
                                          'svenska', 
                                          'svenska', 
                                          'svenska', 
                                          'svenska', 
                                          'svenska', 
                                          'svenska', 
                                          'svenska', 
                                          'svenska', 
                                          'svenska', 
                                          'svenska', 
                                          'svenska', 
                                          'svenska', 
                                          'svenska', 
                                          'svenska', 
                                          'svenska', 
                                          'svenska', 
                                          'svenska', 
                                          'svenska', 
                                          'svenska', 
                                          'svenska', 
                                          'körkort', 
                                          'körkort', 
                                          'körkort', 
                                          'körkort', 
                                          'körkort']
         
        self.assertEqual(connected_concepts_jobadsskill[0], 'svenska')
        self.assertEqual(connected_concepts_jobadsskill[26], 'körkort')
        self.assertEqual(connected_concepts_syndic[0], 'svenska')
        self.assertEqual(connected_concepts_syndic[26], 'körkort')

if __name__ == '__main__':
    unittest.main()
